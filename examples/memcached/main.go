package main

import (
	"log"

	dl "gitlab.com/bf86/lib/go/distributedlocker"
	"gitlab.com/bf86/lib/go/distributedlocker/services"
)

func main() {
	client, err := services.NewMemcachedService()
	if err != nil {
		log.Fatalf("service creation failed: %s", err.Error())
	}
	locker := dl.NewLocker(client)
	lock := locker.NewLock()

	err = locker.Acquire(lock)
	if err != nil {
		log.Fatalf("lock failed: %s", err.Error())
	}

	defer locker.Release(lock)

	// Run your distributed locked code!
}

package lib

import (
	"reflect"
	"testing"
)

func TestNewLock(t *testing.T) {
	type args struct {
		id string
	}
	tests := []struct {
		name    string
		args    args
		want    *Lock
		wantErr bool
	}{
		{name: "Identifier", args: args{id: "test"}, want: &Lock{Identifier: "test"}, wantErr: false},
		{name: "Invalid Identifier", args: args{id: ""}, want: &Lock{}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewLock(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewLock() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewLock() = %v, want %v", got, tt.want)
			}
		})
	}
}

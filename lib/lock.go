package lib

import (
	"fmt"
	"time"
)

/*
Lock provides a nice wrapper for a generic lock type

This can be passed to any distributed locking service
*/
type Lock struct {
	Identifier string        `json:"identifier"`
	Locked     bool          `json:"is_locked"`
	Expiry     time.Duration `json:"expiry"`
}

/* NewLock:

Provides a default wrapper for a lock struct.
*/
func NewLock(id string) (*Lock, error) {
	if id == "" {
		return &Lock{}, fmt.Errorf("you must supply a valid, unique ID")
	}
	return &Lock{Identifier: id}, nil
}

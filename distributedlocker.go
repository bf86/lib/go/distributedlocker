package distributedlocker

import (
	"log"

	"gitlab.com/bf86/lib/go/distributedlocker/interfaces"
	"gitlab.com/bf86/lib/go/distributedlocker/lib"
)

type Locker struct {
	LockService interfaces.LockService
}

func NewLocker(client interfaces.LockService) *Locker {
	return &Locker{LockService: client}
}

func (dl *Locker) NewLock() *lib.Lock {
	return dl.LockService.NewLock("lock_id")
}

func (dl *Locker) Acquire(lock *lib.Lock) error {
	lock, err := dl.LockService.Acquire(lock)
	log.Printf("locked: %t", lock.Locked)
	if err != nil {
		return err
	}
	return nil
}

func (dl *Locker) Release(lock *lib.Lock) {
	lock, _ = dl.LockService.Release(lock)
	log.Printf("locked: %t", lock.Locked)
}

package distributedlocker

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/bf86/lib/go/distributedlocker/interfaces"
	"gitlab.com/bf86/lib/go/distributedlocker/lib"
)

type mockLockService struct {
	Error bool
}

func (m *mockLockService) NewLock(id string) *lib.Lock {
	return &lib.Lock{Identifier: "distlock_" + id}
}
func (m *mockLockService) Acquire(lock *lib.Lock) (*lib.Lock, error) {
	if m.Error {
		return lock, fmt.Errorf("an error")
	}
	lock.Locked = true
	return lock, nil
}
func (m *mockLockService) Release(lock *lib.Lock) (*lib.Lock, error) {
	lock.Locked = false
	return lock, nil
}

func TestNewLocker(t *testing.T) {
	emptyMockService := new(mockLockService)
	type args struct {
		client interfaces.LockService
	}
	tests := []struct {
		name string
		args args
		want *Locker
	}{
		{name: "Get New Locker", args: args{client: emptyMockService}, want: &Locker{LockService: emptyMockService}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewLocker(tt.args.client); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewLocker() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLocker_NewLock(t *testing.T) {
	type fields struct {
		LockService interfaces.LockService
	}
	tests := []struct {
		name   string
		fields fields
		want   *lib.Lock
	}{
		{name: "Create new Lock", fields: fields{LockService: new(mockLockService)}, want: &lib.Lock{Identifier: "distlock_lock_id"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dl := &Locker{
				LockService: tt.fields.LockService,
			}
			if got := dl.NewLock(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Locker.Lock() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLocker_Acquire(t *testing.T) {
	type fields struct {
		LockService interfaces.LockService
	}
	type args struct {
		lock *lib.Lock
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "Aquire Lock",
			fields:  fields{LockService: &mockLockService{}},
			args:    args{lock: &lib.Lock{}},
			wantErr: false,
		},
		{
			name:    "Aquire Lock",
			fields:  fields{LockService: &mockLockService{Error: true}},
			args:    args{lock: &lib.Lock{}},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dl := &Locker{
				LockService: tt.fields.LockService,
			}
			if err := dl.Acquire(tt.args.lock); (err != nil) != tt.wantErr {
				t.Errorf("Locker.Acquire() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestLocker_Release(t *testing.T) {
	type fields struct {
		LockService interfaces.LockService
	}
	type args struct {
		lock *lib.Lock
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "Release Lock",
			fields: fields{LockService: new(mockLockService)},
			args:   args{lock: &lib.Lock{}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dl := &Locker{
				LockService: tt.fields.LockService,
			}
			dl.Release(tt.args.lock)
		})
	}
}

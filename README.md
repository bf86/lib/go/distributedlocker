# Distributed Locker

Simple Distributed Lock Library written in Golang.

### Example usage:

```go
package main

import (
	"log"

	dl "gitlab.com/bf86/lib/go/distributedlocker"
	"gitlab.com/bf86/lib/go/distributedlocker/services"
)

func main() {
	client, err := services.NewMemcachedService()
	if err != nil {
		log.Fatalf("service creation failed: %s", err.Error())
	}
	locker := dl.NewLocker(client)
	lock := locker.NewLock()

	err = locker.Acquire(lock)
	if err != nil {
		log.Fatalf("lock failed: %s", err.Error())
	}

	defer locker.Release(lock)

	// Run your distributed locked code!
}
```

### Local Testing

If you wish to run the tests locally, please spin up the provided `docker-compose.yml` to allow the services to connect to their respective data stores.
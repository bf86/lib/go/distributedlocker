<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by "type::bug" label:

- https://gitlab.com/bf86/lib/go/distributedlocker/issues?label_name%5B%5D=type::bug

and verify the issue you're about to submit isn't a duplicate.
--->

### Summary

<!-- Summarize the bug encountered concisely. -->

### Steps to reproduce

<!-- Describe how one can reproduce the issue - this is very important. Please use an ordered list. -->

### Example Project

<!-- If possible, please create an example project that exhibits the problematic 
behavior, and link to it here in the bug report. If you are using an older version, this 
will also determine whether the bug is fixed in a more recent version. -->

### What is the current *bug* behavior?

<!-- Describe what actually happens. -->

### What is the expected *correct* behavior?

<!-- Describe what you should see instead. -->

### Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code
 as it's tough to read otherwise. -->

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem. -->

/label ~"type::bug"
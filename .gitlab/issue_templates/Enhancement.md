<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by "type::enhancement" label:

- https://gitlab.com/bf86/lib/go/distributedlocker/issues?label_name%5B%5D=type::enhancement

and verify the issue you're about to submit isn't a duplicate.
--->

### Summary

<!-- Summarize the enhancement concisely. -->

### What is the current behavior (if any)?

<!-- Describe your idea. -->

/label ~"type::enhancement"
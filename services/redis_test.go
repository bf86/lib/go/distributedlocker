package services

import (
	"os"
	"reflect"
	"testing"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/google/uuid"
	"gitlab.com/bf86/lib/go/distributedlocker/lib"
)

var key_id = ""
var mockRedisClient = redis.NewClient(&redis.Options{})

func setup() {
	key_id = uuid.NewString()
	ENV_REDIS_ADDRESS = os.Getenv("ENV_REDIS_ADDRESS")
	mockRedisClient = redis.NewClient(&redis.Options{
		Network: "tcp",
		Addr:    ENV_REDIS_ADDRESS,
		DB:      0,
	})
}

func TestNewRedisService_failures(t *testing.T) {
	ENV_REDIS_ADDRESS = ""
	tests := []struct {
		name    string
		want    *RedisService
		wantErr bool
	}{
		{name: "No ENV set", want: &RedisService{}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewRedisService()
			if (err != nil) != tt.wantErr {
				t.Errorf("NewRedisService() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewRedisService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewRedisService(t *testing.T) {
	setup()
	tests := []struct {
		name    string
		want    *RedisService
		wantErr bool
	}{
		{name: "ENV set", want: &RedisService{Client: mockRedisClient}, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := NewRedisService()
			if (err != nil) != tt.wantErr {
				t.Errorf("NewRedisService() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestRedisService_NewLock(t *testing.T) {
	setup()
	type fields struct {
		Client *redis.Client
	}
	type args struct {
		id string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *lib.Lock
	}{
		{
			name:   "Default",
			fields: fields{Client: mockRedisClient},
			args:   args{id: "test"},
			want:   &lib.Lock{Identifier: "distlock_test"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ms := &RedisService{
				Client: tt.fields.Client,
			}
			if got := ms.NewLock(tt.args.id); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RedisService.NewLock() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedisService_Acquire(t *testing.T) {
	setup()

	type fields struct {
		Client *redis.Client
	}
	type args struct {
		lock *lib.Lock
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *lib.Lock
		wantErr bool
	}{
		{
			name: "Successfully Acquire Lock",
			fields: fields{
				Client: mockRedisClient,
			},
			args: args{
				lock: &lib.Lock{Identifier: key_id, Locked: false},
			},
			want:    &lib.Lock{Identifier: key_id, Locked: true},
			wantErr: false,
		},
		{
			name: "Unsuccessfully Acquire Lock",
			fields: fields{
				Client: mockRedisClient,
			},
			args: args{
				lock: &lib.Lock{Identifier: key_id, Locked: false, Expiry: -10 * time.Second},
			},
			want:    &lib.Lock{Identifier: key_id, Locked: false, Expiry: -10 * time.Second},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rs := &RedisService{
				Client: tt.fields.Client,
			}
			got, err := rs.Acquire(tt.args.lock)
			if (err != nil) != tt.wantErr {
				t.Errorf("RedisService.Acquire() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RedisService.Acquire() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedisService_Release(t *testing.T) {
	setup()

	rs, _ := NewRedisService()
	lock, _ := rs.Acquire(&lib.Lock{Identifier: key_id, Locked: true})

	type fields struct {
		Client *redis.Client
	}
	type args struct {
		lock *lib.Lock
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *lib.Lock
		wantErr bool
	}{
		{
			name: "Successfully Release Lock",
			fields: fields{
				Client: mockRedisClient,
			},
			args: args{
				lock: lock,
			},
			want:    &lib.Lock{Identifier: key_id, Locked: false},
			wantErr: false,
		},
		{
			name: "Unsuccessfully Acquire Lock",
			fields: fields{
				Client: mockRedisClient,
			},
			args: args{
				lock: &lib.Lock{Identifier: key_id, Locked: true, Expiry: -10 * time.Second},
			},
			want:    &lib.Lock{Identifier: key_id, Locked: true, Expiry: -10 * time.Second},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rs := &RedisService{
				Client: tt.fields.Client,
			}
			got, err := rs.Release(tt.args.lock)
			if (err != nil) != tt.wantErr {
				t.Errorf("RedisService.Release() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RedisService.Release() = %v, want %v", got, tt.want)
			}
		})
	}
}

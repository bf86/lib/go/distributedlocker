package services

import (
	"context"
	"fmt"
	"os"

	"github.com/go-redis/redis/v8"
	"gitlab.com/bf86/lib/go/distributedlocker/lib"
)

/* RedisService:
A service for creating distributed locks in Redis
*/
type RedisService struct {
	Client *redis.Client
}

var ENV_REDIS_ADDRESS = os.Getenv("ENV_REDIS_ADDRESS")

/* NewRedisService:
Creates a new Redis Service to allow redis locks.

Returns:
		- *RedisService:  // Pointer to the generated RedisService struct.
		- error: // Will error if ENV_REDIS_ADDRESS is not set
*/
func NewRedisService() (*RedisService, error) {
	if ENV_REDIS_ADDRESS == "" {
		return &RedisService{}, fmt.Errorf("ENV_REDIS_ADDRESS not set")
	}
	return &RedisService{
		Client: redis.NewClient(&redis.Options{
			Network: "tcp",
			Addr:    ENV_REDIS_ADDRESS,
			DB:      0,
		}),
	}, nil
}

/* NewLock:
Generate a new Lock struct

Parameters:
		- id (string): Lock Identifier MUST be unique to avoid lock collisions
Returns:
		- *lib.Lock: Pointer to the generated Lock struct to allow for customisation.
*/
func (ms *RedisService) NewLock(id string) *lib.Lock {
	return &lib.Lock{
		Identifier: "distlock_" + id,
		Locked:     false,
		Expiry:     0, // Defaults to never expire so we can defer the release manually
	}
}

/* Acquire a distributed lock in Redis

Parameters:
		- lock (*lib.Lock): // Unique lock struct
Returns:
		- *lib.Lock: // Pointer to the Lock struct
		- error: // Any errors that occured acquiring the lock
*/
func (rs *RedisService) Acquire(lock *lib.Lock) (*lib.Lock, error) {
	ctx := context.Background()
	success, err := rs.Client.SetNX(ctx, lock.Identifier, nil, lock.Expiry).Result()

	lock.Locked = success
	return lock, err
}

/* Release a distributed lock in Redis

Parameters:
		- lock (*lib.Lock): // Unique lock struct
Returns:
		- *lib.Lock: // Pointer to the Lock struct
		- error: // Any errors that occured acquiring the lock
*/
func (rs *RedisService) Release(lock *lib.Lock) (*lib.Lock, error) {
	ctx := context.Background()
	success, err := rs.Client.Del(ctx, lock.Identifier).Result()

	if success > 0 {
		// Lock no longer held
		lock.Locked = false
	} else {
		err = fmt.Errorf("Unable to remove lock: %s", lock.Identifier)
	}
	return lock, err
}

package services

import (
	"fmt"
	"os"

	"github.com/bradfitz/gomemcache/memcache"
	"gitlab.com/bf86/lib/go/distributedlocker/lib"
)

var ENV_MEMCACHED_ADDRESS = os.Getenv("ENV_MEMCACHED_ADDRESS")

type MemcachedService struct {
	Cache *memcache.Client
}

/* NewMemcachedService: Generate a new MemcachedService struct

Returns:
		- *MemcachedService: // Pointer to the generated MemcachedService struct.
		- error: // Will error if ENV_MEMCACHED_ADDRESS isn't set
*/
func NewMemcachedService() (*MemcachedService, error) {
	if ENV_MEMCACHED_ADDRESS == "" {
		return &MemcachedService{}, fmt.Errorf("ENV_MEMCACHED_ADDRESS not set")
	}
	return &MemcachedService{Cache: memcache.New(ENV_MEMCACHED_ADDRESS)}, nil
}

/* NewLock:
Generate a new Lock struct

Parameters:
		- id (string): // Lock Identifier MUST be unique to avoid lock collisions
Returns:
		- *lib.Lock: // Pointer to the generated Lock struct to allow for customisation.
*/
func (ms *MemcachedService) NewLock(id string) *lib.Lock {
	return &lib.Lock{
		Identifier: "distlock_" + id,
		Locked:     false,
		Expiry:     0, // Defaults to never expire so we can defer the release manually
	}
}

/* Acquire a distributed lock in Memcached

Parameters:
		- lock (*lib.Lock): // Unique lock struct
Returns:
		- *lib.Lock: // Pointer to the Lock struct
		- error: // Any errors that occured acquiring the lock
*/
func (ms *MemcachedService) Acquire(lock *lib.Lock) (*lib.Lock, error) {
	item := &memcache.Item{Key: lock.Identifier, Value: []byte{'d'}, Expiration: int32(lock.Expiry)}

	err := ms.Cache.Add(item)
	if err != nil {
		return lock, err
	} else {
		lock.Locked = true
		return lock, nil
	}
}

/* Release a distributed lock in Memcached

Parameters:
		- lock (*lib.Lock): // Unique lock struct
Returns:
		- *lib.Lock: // Pointer to the Lock struct
		- error: // Any errors that occured acquiring the lock
*/
func (ms *MemcachedService) Release(lock *lib.Lock) (*lib.Lock, error) {
	err := ms.Cache.Delete(lock.Identifier)
	if err == nil {
		lock.Locked = false
	}
	return lock, err
}

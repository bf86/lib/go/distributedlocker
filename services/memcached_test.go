package services

import (
	"os"
	"reflect"
	"testing"
	"time"

	"github.com/bradfitz/gomemcache/memcache"
	"github.com/google/uuid"
	"gitlab.com/bf86/lib/go/distributedlocker/lib"
)

var testServer = os.Getenv("ENV_MEMCACHED_ADDRESS")
var mockMemcache = memcache.New(testServer)
var mockMemcacheService = &MemcachedService{Cache: mockMemcache}

func TestNewMemcachedService_failures(t *testing.T) {
	ENV_MEMCACHED_ADDRESS = ""
	tests := []struct {
		name    string
		want    *MemcachedService
		wantErr bool
	}{
		{name: "No ENV set", want: &MemcachedService{}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewMemcachedService()
			if (err != nil) != tt.wantErr {
				t.Errorf("NewMemcachedService() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMemcachedService() = %v, want %v", got, tt.want)
			}
		})
	}
}
func TestNewMemcachedService(t *testing.T) {
	ENV_MEMCACHED_ADDRESS = testServer
	tests := []struct {
		name    string
		want    *MemcachedService
		wantErr bool
	}{
		{name: "ENV set", want: mockMemcacheService, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			got, err := NewMemcachedService()
			if (err != nil) != tt.wantErr {
				t.Errorf("NewMemcachedService() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMemcachedService() = %+v, want %+v", got, tt.want)
			}
		})
	}
}

func TestMemcachedService_NewLock(t *testing.T) {
	type fields struct {
		Cache *memcache.Client
	}
	type args struct {
		id string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *lib.Lock
	}{
		{name: "Default", fields: fields{Cache: mockMemcache}, args: args{id: "test"}, want: &lib.Lock{Identifier: "distlock_test"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ms := &MemcachedService{
				Cache: tt.fields.Cache,
			}
			if got := ms.NewLock(tt.args.id); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MemcachedService.NewLock() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMemcachedService_Acquire(t *testing.T) {
	key_id := uuid.NewString()
	type fields struct {
		Cache *memcache.Client
	}
	type args struct {
		lock *lib.Lock
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *lib.Lock
		wantErr bool
	}{
		{
			name: "Successfully Acquire Lock",
			fields: fields{
				Cache: mockMemcache,
			}, args: args{
				lock: &lib.Lock{Identifier: key_id, Locked: false},
			}, want: &lib.Lock{Identifier: key_id, Locked: true},
			wantErr: false,
		},
		{
			name: "Unsuccessful Lock Attempt",
			fields: fields{
				Cache: mockMemcache,
			},
			args: args{
				lock: &lib.Lock{
					Identifier: key_id,
					Locked:     false,
					Expiry:     -10 * time.Hour,
				},
			},
			want: &lib.Lock{
				Identifier: key_id,
				Locked:     false,
				Expiry:     -10 * time.Hour,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ms := &MemcachedService{
				Cache: tt.fields.Cache,
			}
			got, err := ms.Acquire(tt.args.lock)
			if (err != nil) != tt.wantErr {
				t.Errorf("MemcachedService.Acquire() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MemcachedService.Acquire() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMemcachedService_Release(t *testing.T) {
	key_id := uuid.NewString()

	// Create the key to release
	_, _ = mockMemcacheService.Acquire(&lib.Lock{Identifier: key_id})
	type fields struct {
		Cache *memcache.Client
	}
	type args struct {
		lock *lib.Lock
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *lib.Lock
		wantErr bool
	}{
		{
			name: "Successfully Release",
			fields: fields{
				Cache: mockMemcache,
			},
			args: args{
				lock: &lib.Lock{Identifier: key_id, Locked: true},
			},
			want:    &lib.Lock{Identifier: key_id, Locked: false},
			wantErr: false,
		},
		{
			name: "Unsuccessful Release",
			fields: fields{
				Cache: mockMemcache,
			},
			args: args{
				lock: &lib.Lock{
					Identifier: key_id,
					Locked:     true,
				},
			},
			want: &lib.Lock{
				Identifier: key_id,
				Locked:     true,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ms := &MemcachedService{
				Cache: tt.fields.Cache,
			}
			got, err := ms.Release(tt.args.lock)
			if (err != nil) != tt.wantErr {
				t.Errorf("MemcachedService.Release() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MemcachedService.Release() = %v, want %v", got, tt.want)
			}
		})
	}
}

package interfaces

import (
	"gitlab.com/bf86/lib/go/distributedlocker/lib"
)

/* LockService:

Provides an interface for use with any compatible distributed locking service
*/
type LockService interface {
	NewLock(id string) *lib.Lock
	Acquire(lock *lib.Lock) (*lib.Lock, error)
	Release(lock *lib.Lock) (*lib.Lock, error)
}
